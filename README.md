# sphinx aimms domain

A domain for Sphinx Python Documentation generator to support AMMS language objects.

How to install it
-------------------

In the sphinx setup (at the end of the conf.py file of your sphinx repo), input the following:

``` python

def setup(sphinx):
	
    sys.path.insert(0, os.path.abspath("includes")) # your path to the AIMMSdomain.py file. We chose to put it in an "includes" folder located in the sphinx repo.
    from AIMMSDomain import AIMMSDomain
    sphinx.add_domain(AIMMSDomain)
    print "\nAIMMS Domain added"
 
 ```
 
 That should add this domain to your sphinx syntax
 
 
What syntax are we talking about
-----------------------------------

If the import is successful, you will be able now to refer to the ``:aimms:`` domain in your RST documentation as follows

``` rst

.. aimms:elementparameter::`EP_Whatever`
    :attribute indexDomain:`(i,j)`
    :attribute range:`Set1`

bla bla bla. bla bla. bla.

:aimms:stringparameter:`SP_Whatever`

```

Seealso
---------

https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html

Example
----------

see wiki https://gitlab.com/ArthurdHerbemont/sphinx-aimms-domain/wikis/use-case-examples



 
 